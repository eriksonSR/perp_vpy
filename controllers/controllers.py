from prettytable import PrettyTable
from models.models import *
from apis.apis import *
import pprint, json, requests, os

class AutoresController():
	def __init__(self):
		self.ExibeMenu()

	def ExibeMenu(self):
		msg = """\
Dite o código da opção desejada:
1 - Listar autores;
2 - Listar livros do autor;
3 - Cadastrar novo autor;
0 - Sair
"""
		m_autores = AutoresModel()
		op = ''
		while op != 0:
			op = int(input(msg))
			if op == 1:
				autores = m_autores.Listar()
				tb = PrettyTable(['ID', 'AUTOR'])

				for a in autores:
					tb.add_row([a[0], a[1]])

				print(tb.get_string())

			elif op == 2:
				print('Listando livros do autor...')
			elif op == 3:
				print('Cadastrando novo autor...')

class EditorasController():
	def __init__(self):
		self.ExibeMenu()

	def ExibeMenu(self):
		msg = """\
Dite o código da opção desejada:
1 - Listar editoras;
2 - Listar livros da editora;
3 - Cadastrar nova editora;
0 - Sair
"""
		m_editoras = EditorasModel()
		op = ''
		while op != 0:
			op = int(input(msg))
			if op == 1:
				editoras = m_editoras.Listar()
				tb = PrettyTable(['ID', 'EDITORA'])

				for e in editoras:
					tb.add_row([e[0], e[1]])

				print(tb.get_string())
			elif op == 2:
				print('Listando livros da editora...')
			elif op == 3:
				print('Cadastrando nova editora...')

class AssuntosController():
	def __init__(self):
		self.ExibeMenu()

	def ExibeMenu(self):
		msg = """\
Dite o código da opção desejada:
1 - Listar assuntos;
2 - Listar livros que tenham o assunto;
3 - Cadastrar novo assunto;
4 - Buscar assunto;
0 - Sair
"""
		m_assuntos = AssuntosModel()
		op = ''
		while op != 0:
			op = int(input(msg))
			if op == 1:
				assuntos = m_assuntos.Listar()
				tb = PrettyTable(['ID', 'ASSUNTO'])

				for a in assuntos:
					tb.add_row([a[0], a[1]])

				print(tb.get_string())
			elif op == 2:
				print('Listando livros do assunto...')
			elif op == 3:
				assunto_nome = input('Informe o assunto: ')
				m_assuntos.Inserir(assunto_nome)
			elif op == 4:
				assunto = input('Informe o assunto: ')
				assuntos = m_assuntos.GetAssuntoByNome(assunto)

				if assuntos:
					tb = PrettyTable(['ID', 'ASSUNTO'])

					for a in assuntos:
						tb.add_row([a[0], a[1]])

					print(tb.get_string())
				else:
					print('Nenhum assunto encontrado...\r\n')

class GenerosController():
	def __init__(self):
		self.ExibeMenu()

	def ExibeMenu(self):
		msg = """\
Dite o código da opção desejada:
1 - Listar generos;
2 - Cadastrar novo genero;
3 - Buscar genero;
0 - Sair
"""
		m_generos = GenerosModel()
		op = ''
		while op != 0:
			op = int(input(msg))
			if op == 1:
				generos = m_generos.Listar()
				tb = PrettyTable(['ID', 'GENERO'])

				for a in generos:
					tb.add_row([a[0], a[1]])

				print(tb.get_string())
			elif op == 2:
				genero_nome = input('Informe o genero: ')
				m_generos.Inserir(genero_nome)
			elif op == 3:
				genero = input('Informe o genero: ')
				generos = m_generos.GetGenerosByNome(genero)

				if generos:
					tb = PrettyTable(['ID', 'GENERO'])

					for a in generos:
						tb.add_row([a[0], a[1]])

					print(tb.get_string())
				else:
					print('Nenhum genero encontrado...\r\n')

class StudiosController():
	def __init__(self):
		self.ExibeMenu()

	def ExibeMenu(self):
		msg = """\
Dite o código da opção desejada:
1 - Listar studios;
2 - Cadastrar novo studio;
3 - Buscar studio;
0 - Sair
"""
		m_studios = StudiosModel()
		op = ''
		while op != 0:
			op = int(input(msg))
			if op == 1:
				studios = m_studios.Listar()
				tb = PrettyTable(['ID', 'GENERO'])

				for s in studios:
					tb.add_row([s[0], s[1]])

				print(tb.get_string())
			elif op == 2:
				studio = input('Informe o studio: ')
				m_studios.Inserir(studio)
			elif op == 3:
				studio = input('Informe o studio: ')
				generos = m_studios.GetStudiosByNome(studio)

				if generos:
					tb = PrettyTable(['ID', 'STUDIO'])

					for s in generos:
						tb.add_row([s[0], s[1]])

					print(tb.get_string())
				else:
					print('Nenhum studio encontrado...\r\n')

class ConsolesController():
	def __init__(self):
		self.ExibeMenu()

	def ExibeMenu(self):
		msg = """\
Dite o código da opção desejada:
1 - Listar consoles;
0 - Sair
"""
		
		m_consoles = ConsolesModel()
		
		op = ''
		while op != 0:
			op = int(input(msg))
			if op == 1:
				consoles = m_consoles.Listar()
				tb = PrettyTable(['ID', 'CONSOLE'])

				for c in consoles:
					tb.add_row([c[0], c[1]])

				print(tb.get_string())

class RelatoriosGamesController():
	def __init__(self):
		self.m_rel_games = GamesRelatoriosModel()
		self.m_plataformas = PlataformasModel()
		self.m_consoles = ConsolesModel()
		self.m_studios = StudiosModel()
		self.m_generos = GenerosModel()
		self.ExibeMenu()

	def ExibeMenu(self):
		msg = """\
Dite o código da opção desejada:
1 - Consolidado por plataforma;
2 - Games por plataforma;
3 - Consolidado por plataforma ao ano;
4 - Games por plataforma ao ano;
5 - Consolidado por console;
6 - Consolidado por console da plataforma;
7 - Games por console da plataforma;
8 - Consolidado por publicadora;
9 - Games por publicadora;
10 - Consolidado por desenvolvedora;
11 - Games por desenvolvedora;
12 - Consolidado por gênero;
13 - Games por gênero;
14 - DLC x Jogo Base;
0 - Sair
"""

		op = ''
		while op != 0:
			op = int(input(msg))
			if op == 1:
				self.ConsolidadoPorPlataforma()
			if op == 2:
				self.GamesPorPlataforma()
			if op == 3:
				self.ConsolidadoPorPlataformaAoAno()
			if op == 4:
				self.GamesPorPlataformaAoAno()
			if op == 5:
				self.ConsolidadoPorConsole()
			if op == 6:
				self.ConsolidadoPorConsoleDaPlataforma()
			if op == 7:
				self.GamesPorConsoleDaPlataforma()
			if op == 8:
				self.ConsolidadoPorPublicadora()
			if op == 9:
				self.GamesPorPublicadora()
			if op == 10:
				self.ConsolidadoPorDesenvolvedora()
			if op == 11:
				self.GamesPorDesenvolvedora()
			if op == 12:
				self.ConsolidadoPorGenero()
			if op == 13:
				self.GamesPorGenero()
			if op == 14:
				self.DlcXJogoBase()

	def ConsolidadoPorPlataforma(self):
		ft = str(input("Deseja filtrar por plataforma específica (s/n)?"))
		dados = ''
				
		if ft == "s":
			plataformas = self.m_plataformas.Listar()
			tb = PrettyTable(['ID', 'PLATAFORMA'])

			for p in plataformas:
				tb.add_row([p[0], p[1]])

			print(tb.get_string())

			id_p = int(input("Digite o id da plataforma: "))
			dados = self.m_rel_games.ConsolidadoPorPlataforma(id_p)

		else:
			dados = self.m_rel_games.ConsolidadoPorPlataforma()
				
		tb = PrettyTable(["PLATAFORMA","TOTAL"])
		for l in dados:
			tb.add_row([l['plataforma'], l['total']])
				
		print(tb.get_string())

	def GamesPorPlataforma(self):
		ft = str(input("Deseja filtrar por plataforma específica (s/n)?"))
		dados = ''
				
		if ft == "s":
			plataformas = self.m_plataformas.Listar()
			tb = PrettyTable(['ID', 'PLATAFORMA'])

			for p in plataformas:
				tb.add_row([p[0], p[1]])

			print(tb.get_string())

			id_p = int(input("Digite o id da plataforma: "))
			dados = self.m_rel_games.GamesPorPlataforma(id_p)

		else:
			dados = self.m_rel_games.GamesPorPlataforma()
		

		tb = PrettyTable(['PLATAFORMA', 'CONSOLE', 'TITULO'])
		for l in dados:
			tb.add_row([l['plataforma'], l['console'], l['titulo']])
				
		print(tb.get_string())

	def ConsolidadoPorPlataformaAoAno(self):
		ft_plataforma = str(input("Deseja filtrar por plataforma específica (s/n)?"))
		ft_ano = str(input("Deseja filtrar por ano específico (s/n)?"))
		dados = ''
				
		if ft_plataforma == "s" and ft_ano == "s":
			plataformas = self.m_plataformas.Listar()
			tb = PrettyTable(['ID', 'PLATAFORMA'])

			for p in plataformas:
				tb.add_row([p[0], p[1]])

			print(tb.get_string())

			id_plataforma = int(input("Digite o id da plataforma: "))
			ano = int(input("Informe o ano: "))
			dados = self.m_rel_games.ConsolidadoPorPlataformaAoAno(id_plataforma = id_plataforma, ano = ano)

		elif ft_plataforma == "s" and ft_ano == "n":
			plataformas = self.m_plataformas.Listar()
			tb = PrettyTable(['ID', 'PLATAFORMA'])

			for p in plataformas:
				tb.add_row([p[0], p[1]])

			print(tb.get_string())

			id_plataforma = int(input("Digite o id da plataforma: "))
			dados = m_rel_games.ConsolidadoPorPlataformaAoAno(id_plataforma = id_plataforma)

		elif ft_ano == "s" and ft_plataforma == "n":
			ano = int(input("Informe o ano: "))
			dados = self.m_rel_games.ConsolidadoPorPlataformaAoAno(ano = ano)

		else:
			dados = self.m_rel_games.ConsolidadoPorPlataformaAoAno()
				
		tb = PrettyTable(['PLATAFORMA', 'ANO', 'TOTAL'])
		for l in dados:
			tb.add_row([l['plataforma'], l['ano'], l['total']])
				
		print(tb.get_string())

	def GamesPorPlataformaAoAno(self):
		ft_plataforma = str(input("Deseja filtrar por plataforma específica (s/n)?"))
		ft_ano = str(input("Deseja filtrar por ano específico (s/n)?"))
		dados = ''
				
		if ft_plataforma == "s" and ft_ano == "s":
			plataformas = self.m_plataformas.Listar()
			tb = PrettyTable(['ID', 'PLATAFORMA'])

			for p in plataformas:
				tb.add_row([p[0], p[1]])

			print(tb.get_string())

			id_plataforma = int(input("Digite o id da plataforma: "))
			ano = int(input("Informe o ano: "))
			dados = self.m_rel_games.GamesPorPlataformaAoAno(id_plataforma = id_plataforma, ano = ano)

		elif ft_plataforma == "s" and ft_ano == "n":
			plataformas = self.m_plataformas.Listar()
			tb = PrettyTable(['ID', 'PLATAFORMA'])

			for p in plataformas:
				tb.add_row([p[0], p[1]])

			print(tb.get_string())

			id_plataforma = int(input("Digite o id da plataforma: "))
			dados = self.m_rel_games.GamesPorPlataformaAoAno(id_plataforma = id_plataforma)

		elif ft_ano == "s" and ft_plataforma == "n":
			ano = int(input("Informe o ano: "))
			dados = self.m_rel_games.GamesPorPlataformaAoAno(ano = ano)

		else:
			dados = self.m_rel_games.GamesPorPlataformaAoAno()
				
		tb = PrettyTable(['PLATAFORMA', 'ANO', 'TITULO'])
		for l in dados:
			tb.add_row([l['plataforma'], l['ano'], l['titulo']])
				
		print(tb.get_string())

	def ConsolidadoPorConsole(self):
		ft_console = str(input("Deseja filtrar por console específico (s/n)?"))
		dados = ''
				
		if ft_console == "s":
			consoles = self.m_consoles.Listar()
			tb = PrettyTable(['ID', 'CONSOLE'])

			for c in consoles:
				tb.add_row([c[0], c[1]])

			print(tb.get_string())

			id_console = int(input("Digite o id do console: "))
			dados = self.m_rel_games.ConsolidadoPorConsole(id_console = id_console)

		else:
			dados = self.m_rel_games.ConsolidadoPorConsole()
				
		tb = PrettyTable(['CONSOLE', 'TOTAL'])
		for l in dados:
			tb.add_row([l['console'], l['total']])
				
		print(tb.get_string())

	def ConsolidadoPorConsoleDaPlataforma(self):
		ft_plataforma = str(input("Deseja filtrar por plataforma específica (s/n)?"))
		dados = ''
				
		if ft_plataforma == "s":
			plataformas = self.m_plataformas.Listar()
			tb = PrettyTable(['ID', 'PLATAFORMA'])

			for p in plataformas:
				tb.add_row([p[0], p[1]])

			print(tb.get_string())

			id_plataforma = int(input("Digite o id da plataforma: "))
			dados = self.m_rel_games.ConsolidadoPorConsoleDaPlataforma(id_plataforma = id_plataforma)

		else:
			dados = self.m_rel_games.ConsolidadoPorConsoleDaPlataforma()
				
		tb = PrettyTable(['PLATAFORMA', 'CONSOLE', 'TOTAL'])	
		for l in dados:
			tb.add_row([l['plataforma'],l['console'],l['total']])
				
		print(tb.get_string())

	def GamesPorConsoleDaPlataforma(self):
		ft_plataforma = str(input("Deseja filtrar por plataforma específica (s/n)?"))
		dados = ''
				
		if ft_plataforma == "s":
			plataformas = self.m_plataformas.Listar()
			tb = PrettyTable(['ID', 'PLATAFORMA'])

			for p in plataformas:
				tb.add_row([p[0], p[1]])

			print(tb.get_string())

			id_plataforma = int(input("Digite o id da plataforma: "))
			dados = self.m_rel_games.GamesPorConsoleDaPlataforma(id_plataforma = id_plataforma)

		else:
			dados = self.m_rel_games.GamesPorConsoleDaPlataforma()
				
		tb = PrettyTable(['PLATAFORMA', 'CONSOLE', 'TITULO'])	
		for l in dados:
			tb.add_row([l['plataforma'],l['console'],l['titulo']])
				
		print(tb.get_string())

	def GamesPorPublicadora(self):
		ft_publicadora = str(input("Deseja filtrar por publicadora específica (s/n)?"))
		dados = ''
				
		if ft_publicadora == "s":
			studios = self.m_studios.Listar()
			tb = PrettyTable(['ID', 'PUBLICADORA'])

			for s in studios:
				tb.add_row([s[0], s[1]])

			print(tb.get_string())

			id_studio = int(input("Digite o id da publicadora: "))
			dados = self.m_rel_games.GamesPorPublicadora(id_publicadora = id_studio)

		else:
			dados = self.m_rel_games.GamesPorPublicadora()
				
		tb = PrettyTable(['PUBLICADORA', 'TITULO'])
				
		for l in dados:
			tb.add_row([l['publicadora'],l['titulo']])
				
		print(tb.get_string())

	def ConsolidadoPorPublicadora(self):
		ft_publicadora = str(input("Deseja filtrar por publicadora específica (s/n)?"))
		dados = ''
				
		if ft_publicadora == "s":
			studios = self.m_studios.Listar()
			tb = PrettyTable(['ID', 'PUBLICADORA'])

			for s in studios:
				tb.add_row([s[0], s[1]])

			print(tb.get_string())

			id_studio = int(input("Digite o id da publicadora: "))
			dados = self.m_rel_games.ConsolidadoPorPublicadora(id_publicadora = id_studio)

		else:
			dados = self.m_rel_games.ConsolidadoPorPublicadora()
				
			
		tb = PrettyTable(['PUBLICADORA', 'TOTAL'])	
		for l in dados:
			tb.add_row([l['publicadora'],l['total']])

		print(tb.get_string())

	def ConsolidadoPorDesenvolvedora(self):
		ft_desenvolvedora = str(input("Deseja filtrar por desenvolvedora específica (s/n)?"))
		dados = ''
				
		if ft_desenvolvedora == "s":
			studios = self.m_studios.Listar()
			tb = PrettyTable(['ID', 'DESENVOLVEDORA'])

			for s in studios:
				tb.add_row([s[0], s[1]])

			print(tb.get_string())

			id_studio = int(input("Digite o id da desenvolvedora: "))
			dados = self.m_rel_games.ConsolidadoPorDesenvolvedora(id_desenvolvedora = id_studio)
		else:
			dados = self.m_rel_games.ConsolidadoPorDesenvolvedora()
		
		tb = PrettyTable(['DESENVOLVEDORA', 'TOTAL'])
		for l in dados:
			tb.add_row([l['desenvolvedora'],l['total']])

		print(tb.get_string())

	def GamesPorDesenvolvedora(self):
		ft_desenvolvedora = str(input("Deseja filtrar por desenvolvedora específica (s/n)?"))
		dados = ''
				
		if ft_desenvolvedora == "s":
			studios = self.m_studios.Listar()
			tb = PrettyTable(['ID', 'DESENVOLVEDORA'])

			for s in studios:
				tb.add_row([s[0], s[1]])

			print(tb.get_string())

			id_studio = int(input("Digite o id da desenvolvedora: "))
			dados = self.m_rel_games.GamesPorDesenvolvedora(id_desenvolvedora = id_studio)

		else:
			dados = self.m_rel_games.GamesPorDesenvolvedora()
				
		tb = PrettyTable(['DESENVOLVEDORA', 'TITULO'])	
		for l in dados:
			tb.add_row([l['desenvolvedora'],l['titulo']])

		print(tb.get_string())

	def ConsolidadoPorGenero(self):
		ft_genero = str(input("Deseja filtrar por gênero específico (s/n)?"))
		dados = ''
				
		if ft_genero == "s":
			generos = self.m_generos.Listar()
			tb = PrettyTable(['ID', 'GENERO'])

			for g in generos:
				tb.add_row([g[0], g[1]])

			print(tb.get_string())

			id_genero = int(input("Digite o id do gênero: "))
			dados = self.m_rel_games.ConsolidadoPorGenero(id_genero = id_genero)

		else:
			dados = self.m_rel_games.ConsolidadoPorGenero()
				
		tb = PrettyTable(['GENERO', 'TOTAL'])
		for l in dados:
			tb.add_row([l['genero'],l['total']])

		print(tb.get_string())

	def GamesPorGenero(self):
		ft_genero = str(input("Deseja filtrar por gênero específico (s/n)?"))
		dados = ''
				
		if ft_genero == "s":
			generos = self.m_generos.Listar()
			tb = PrettyTable(['ID', 'GENERO'])

			for g in generos:
				tb.add_row([g[0], g[1]])

			print(tb.get_string())

			id_genero = int(input("Digite o id do gênero: "))
			dados = self.m_rel_games.GamesPorGenero(id_genero = id_genero)

		else:
			dados = self.m_rel_games.GamesPorGenero()
				
		tb = PrettyTable(['GENERO', 'TITULO'])
		for l in dados:
			tb.add_row([l['genero'],l['titulo']])

		print(tb.get_string())

	def DlcXJogoBase(self):
		dados = self.m_rel_games.GamesDlcXjogoBase()
				
		tb = PrettyTable(['TIPO', 'TOTAL'])
				
		for l in dados:
			tb.add_row([l['tipo'],l['total']])

		print(tb.get_string())

class LeiturasController():
	def __init__(self):
		self.m_leituras = LeiturasModel()
		self.m_autores = AutoresModel()
		self.m_assuntos = AssuntosModel()
		self.m_editoras = EditorasModel()
		self.dir_json = './dados_apis/skoob/json/'
		self.editoras = []
		self.autores = []
		self.assuntos = []
		self.ExibeMenu()

	def ExibeMenu(self):
		msg = """\
Dite o código da opção desejada:
1 - Inserir;
0 - Sair
"""

		op = ''
		while op != 0:
			op = int(input(msg))
			if op == 1:
				leitura = {
					'autores': [],
					'editoras': [],
					'assuntos': [],
					'tipo_midia': 0,
					'id_skoob': 0,
					'digital': 0,
					'favorito': 0,
					'dt_fim': '',
					'nota': 0,
					'pags': 0,
					'resenha': '',
					'sinopse': '',
					'titulo': '',
					'subtitulo': '',
					'ano': 0,
					'isbn': 0,
					'marcacoes': {},
					'capas': {'grande':'', 'media':'', 'pequena':'', 'micro':'', 'nano':''},
				}

				leitura['id_skoob'] = int(input("Informe o id da leitura no Skoob: "))
				api = SkoobApi(leitura['id_skoob'], 1)
				capas = api.GetCapasUrls()

				arq = r'{}{}.json'.format(self.dir_json, leitura['id_skoob'])
				with open(arq, 'r') as arq_j:
					j = json.load(arq_j)
					leitura['sinopse'] = j['response']['sinopse']
					leitura['pags'] = j['response']['paginas']
					leitura['ano'] = j['response']['ano']
					leitura['titulo'] = j['response']['titulo']
					leitura['dt_fim'] = '2018-08-10'
					#leitura['dt_fim'] = j['response']['meu_livro']['dt_leitura']
					leitura['favorito'] = j['response']['meu_livro']['favorito']
					leitura['nota'] = j['response']['meu_livro']['ranking']

				leitura['capas']['grande'] = capas['capa_grande']
				leitura['capas']['media'] = capas['capa_media']
				leitura['capas']['pequena'] = capas['capa_pequena']
				leitura['capas']['micro'] = capas['capa_micro']
				leitura['capas']['nano'] = capas['capa_nano']

				digital = str(input("Digital (s/n): "))
				if digital == "s":
					leitura['digital'] = 1
				else:
					leitura['digital'] = 0

				leitura['isbn'] = int(input("Informe o ISBN: "))
				leitura['tipo_midia'] = int(input("Tipo de midia (1 - Livro, 2 - HQ, 3 - Mangá): "))
				
				self.InserirEditora(1)
				leitura['editoras'] = self.editoras
				
				self.InserirAutor(1)
				leitura['autores'] = self.autores
				
				self.InserirAssunto(1)
				leitura['assuntos'] = self.assuntos

				self.m_leituras.Inserir(**leitura)
				self.BaixarCapas(capas, leitura['id_skoob'])

	def InserirEditora(self, exibe_lista = 0):
		if exibe_lista == 1:
			editoras = self.m_editoras.Listar()
			tb = PrettyTable(['ID', 'EDITORA'])

			for e in editoras:
				tb.add_row([e[0], e[1]])

			print(tb.get_string())
	
		id = int(input("Informe o id da editora: "))
		self.editoras.append(id)
		
		while str(input("Inserir mais editoras (s/n)? ")) == "s":
			self.InserirEditora()

	def InserirAutor(self, exibe_lista = 0):
		if exibe_lista == 1:
			autores = self.m_autores.Listar()
			tb = PrettyTable(['ID', 'AUTOR'])

			for a in autores:
				tb.add_row([a[0], a[1]])

			print(tb.get_string())
	
		id = int(input("Informe o id do autor: "))
		self.autores.append(id)

		while str(input("Inserir mais autores (s/n)? ")) != "n":
			self.InserirAutor()

	def InserirAssunto(self, exibe_lista = 0):
		if exibe_lista == 1:
			assuntos = self.m_assuntos.Listar()
			tb = PrettyTable(['ID', 'ASSUNTO'])

			for a in assuntos:
				tb.add_row([a[0], a[1]])

			print(tb.get_string())
	
		id = int(input("Informe o id do assunto: "))
		self.assuntos.append(id)

		while str(input("Inserir mais assunto (s/n)? ")) != "n":
			self.InserirAssunto()

	def BaixarCapas(self, capas, id_skoob):
		diretorio = r'./imgs/leituras/{}'.format(id_skoob)
			
		if not os.path.exists(diretorio):
			os.makedirs(diretorio)

		print('Baixando imagens...')

		arq_grande = r'./imgs/leituras/{}/capa_grande.jpg'.format(id_skoob)
		arq_media = r'./imgs/leituras/{}/capa_media.jpg'.format(id_skoob)
		arq_pequena = r'./imgs/leituras/{}/capa_pequena.jpg'.format(id_skoob)
		arq_micro = r'./imgs/leituras/{}/capa_micro.jpg'.format(id_skoob)
		arq_nano = r'./imgs/leituras/{}/capa_nano.jpg'.format(id_skoob)

		img_grande = requests.get(capas['capa_grande']).content
		img_media = requests.get(capas['capa_media']).content
		img_pequena = requests.get(capas['capa_pequena']).content
		img_micro = requests.get(capas['capa_micro']).content
		img_nano = requests.get(capas['capa_nano']).content
			
		with open(arq_grande, 'wb') as handler:
			handler.write(img_grande)
			
		with open(arq_media, 'wb') as handler:
		    handler.write(img_media)
			
		with open(arq_pequena, 'wb') as handler:
		    handler.write(img_pequena)
			
		with open(arq_micro, 'wb') as handler:
		    handler.write(img_micro)
		
		with open(arq_nano, 'wb') as handler:
		    handler.write(img_nano)

class GamesController():
	def __init__(self):
		self.m_studios = StudiosModel()
		self.m_consoles = ConsolesModel()
		self.m_generos = GenerosModel()
		self.m_games = GamesModel()
		self.m_consoles = ConsolesModel()
		self.id_igdb = 0
		self.id_gdb = 0
		self.api_igdb = ''
		self.api_gdb = ''
		self.url_imgs = {'boxes':[], 'screens':[], 'arts':[]}
		self.publicadoras = []
		self.desenvolvedoras = []
		self.generos = []
		self.header = {'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/68.0.3440.75 Chrome/68.0.3440.75 Safari/537.36'}

		self.ExibeMenu()

	def ExibeMenu(self):
		msg = """\
Dite o código da opção desejada:
1 - Inserir;
0 - Sair
"""
		op = ''
		while op != 0:
			op = int(input(msg))
			if op == 1:
				game = {
					'id_igdb':0,
					'id_gdb':0,
					'id_console':0,
					'sinopse':'',
					'digital':0,
					'dlc':0,
					'videos':{},
					'titulo':'',
					'dt_finalizacao':'',
					'imagens':{},
					'publicadoras':[],
					'desenvolvedoras':[],
					'generos':[]
				}
				game['id_igdb'] = int(input('Informe o id no IGDB: '))
				game['id_gdb'] = int(input('Informe o id GDB: '))
				self.id_igdb = game['id_igdb']
				self.id_gdb = game['id_gdb']
				
				self.api_igdb = IgdbApi(game['id_igdb'])
				self.api_gdb = GamesDataBaseApi(game['id_gdb'])

				print('Baixando json do Internet Games Database...')
				self.api_igdb.GetJson()
				print('Baixando json do The Games Database...')
				self.api_gdb.GetJson()
				self.GetImages()
				game['imagens'] = self.url_imgs;
				game['videos'] = self.api_igdb.GetVideosUrls()
				game['sinopse'] = self.api_igdb.GetSummary()

				dlc = str(input('DLC (s/n): '))
				if dlc == 's':
					game['dlc'] = 1

				digital = str(input('Digital (s/n): '))
				if digital == 's':
					game['digital'] = 1

				game['titulo'] = str(input('Titulo do jogo: '))
				game['dt_finalizacao'] = str(input('Data de finalização (AAAA-MM-DD): '))

				consoles = self.m_consoles.Listar()
				tb = PrettyTable(['ID', 'CONSOLE'])
				for c in consoles:
					tb.add_row([c[0], c[1]])
				print(tb.get_string())
				game['id_console'] = int(input('Informe o ID do console: '))

				self.InserirPublicadora(1)
				game['publicadoras'] = self.publicadoras

				self.InserirDesenvolvedora(1)
				game['desenvolvedoras'] = self.desenvolvedoras

				self.InserirGenero(1)
				game['generos'] = self.generos

				self.m_games.Inserir(**game)

	def GetImages(self):
		box_igdb = self.api_igdb.GetBoxArtsUrls()
		screens_igdb = self.api_igdb.GetScreenshotsUrls()
		arts_igdb = self.api_igdb.GetArtWorksUrls()

		box_gdb = self.api_gdb.GetBoxArtsUrls()
		screens_gdb = self.api_gdb.GetScreenshotsUrls()
		arts_gdb = self.api_gdb.GetArtWorksUrls()
		
		for i in box_igdb['originais']:
			self.url_imgs['boxes'].append(i)
		for i in screens_igdb['originais']:
			self.url_imgs['screens'].append(i)
		for i in arts_igdb['originais']:
			self.url_imgs['arts'].append(i)
		
		for i in box_gdb['originais']:
			self.url_imgs['boxes'].append(i)
		for i in screens_gdb['originais']:
			self.url_imgs['screens'].append(i)
		for i in arts_gdb['originais']:
			self.url_imgs['arts'].append(i)

		print('Baixando capas...')
		cont = 0
		for i in self.url_imgs['boxes']:
			cont = cont + 1
			url_parts = i.split('/')
			dt_img = url_parts[-1].split('.')
			img_ext = dt_img[1]
			print('URL: {}'.format(i))
			print('CAMINHO: {}'.format('./imgs/games/boxarts/originais/{}-{}.{}'.format(self.id_igdb, cont, img_ext)))
			with open(r'./imgs/games/boxarts/originais/{}-{}.{}'.format(self.id_igdb, cont, img_ext), 'wb') as handle:
				response = requests.get(i, headers=self.header, stream=True)
				print('Resposta: {}'.format(response.status_code))
				handle.write(response.content)

		print('Baixando screenshots...')
		cont = 0
		for i in self.url_imgs['screens']:
			cont = cont + 1
			url_parts = i.split('/')
			dt_img = url_parts[-1].split('.')
			img_ext = dt_img[1]
			print('URL: {}'.format(i))
			print('CAMINHO: {}'.format('./imgs/games/screenshots/originais/{}-{}.{}'.format(self.id_igdb, cont, img_ext)))
			with open(r'./imgs/games/screenshots/originais/{}-{}.{}'.format(self.id_igdb, cont, img_ext), 'wb') as handle:
				response = requests.get(i, headers=self.header, stream=True)
				print('Resposta: {}'.format(response.status_code))
				handle.write(response.content)

		print('Baixando fanarts...')
		cont = 0
		for i in self.url_imgs['arts']:
			cont = cont + 1
			url_parts = i.split('/')
			dt_img = url_parts[-1].split('.')
			img_ext = dt_img[1]
			print('URL: {}'.format(i))
			print('CAMINHO: {}'.format('./imgs/games/fanarts/originais/{}-{}.{}'.format(self.id_igdb, cont, img_ext)))
			with open(r'./imgs/games/fanarts/originais/{}-{}.{}'.format(self.id_igdb, cont, img_ext), 'wb') as handle:
				response = requests.get(i, headers=self.header, stream=True)
				print('Resposta: {}'.format(response.status_code))
				handle.write(response.content)

	def InserirPublicadora(self, exibe_lista = 0):
		if exibe_lista == 1:
			publicadoras = self.m_studios.Listar()
			tb = PrettyTable(['ID', 'PUBLICADORA'])

			for e in publicadoras:
				tb.add_row([e[0], e[1]])

			print(tb.get_string())
	
		id = int(input("Informe o id da publicadora: "))
		self.publicadoras.append(id)
		
		while str(input("Inserir mais publicadoras (s/n)? ")) == "s":
			self.InserirPublicadora()

	def InserirDesenvolvedora(self, exibe_lista = 0):
		if exibe_lista == 1:
			desenvolvedoras = self.m_studios.Listar()
			tb = PrettyTable(['ID', 'DESENVOLVEDORA'])

			for e in desenvolvedoras:
				tb.add_row([e[0], e[1]])

			print(tb.get_string())
	
		id = int(input("Informe o id da desenvolvedora: "))
		self.desenvolvedoras.append(id)
		
		while str(input("Inserir mais desenvolvedoras (s/n)? ")) == "s":
			self.InserirDesenvolvedora()

	def InserirGenero(self, exibe_lista = 0):
		if exibe_lista == 1:
			generos = self.m_generos.Listar()
			tb = PrettyTable(['ID', 'GENERO'])

			for e in generos:
				tb.add_row([e[0], e[1]])

			print(tb.get_string())
	
		id = int(input("Informe o id do genêro: "))
		self.generos.append(id)
		
		while str(input("Inserir mais generos (s/n)? ")) == "s":
			self.InserirGenero()

class RelatoriosLeiturasController():
	def __init__(self):
		self.m_rel_leituras = LeiturasRelatoriosModel()
		self.m_autores = AutoresModel()
		self.m_editoras = EditorasModel()
		self.m_assuntos = AssuntosModel()
		self.m_tipos_midias = TiposMidiasModel()

		self.ExibeMenu()

	def ExibeMenu(self):
		msg = """\
Dite o código da opção desejada:
00 - Listagem geral;
1 - Consolidado por ano;
2 - Leituras por ano;
3 - Consolidado por autor;
4 - Leituras por autor;
5 - Consolidado por editora;
6 - Leituras por editora;
7 - Consolidado por assunto;
8 - Leituras por assunto;
9 - Consolidado por formato;
10 - Leituras por formato;
11 - Consolidado por tipo;
12 - Leituras por tipo;
0 - Sair
"""
		self.m_rel_leituras = LeiturasRelatoriosModel()
		self.m_autores = AutoresModel()
		self.m_editoras = EditorasModel()
		self.m_assuntos = AssuntosModel()
		self.m_tipos_midias = TiposMidiasModel()

		op = ''
		while op != 0:
			op = int(input(msg))
			if op == 1:
				self.ConsolidadoPorAno()
			if op == 2:
				self.LeiturasPorAno()
			if op == 3:
				self.ConsolidadoPorAutor()
			if op == 4:
				self.LeiturasPorAutor()
			if op == 5:
				self.ConsolidadoPorEditora()
			if op == 6:
				self.LeiturasPorEditora()
			if op == 7:
				self.ConsolidadoPorAssunto()
			if op == 8:
				self.LeiturasPorAssunto()
			if op == 9:
				self.ConsolidadoPorFormato()
			if op == 10:
				self.LeiturasPorFormato()
			if op == 11:
				self.ConsolidadoPorTipo()
			if op == 12:
				self.LeiturasPorTipo()
			if op == 00:
				self.ListagemGeral()

	def ConsolidadoPorAno(self):
		ft_ano = str(input("Deseja filtrar por ano específico (s/n)?"))
		dados = ''
				
		if ft_ano == "s":
			ano = int(input("Digite o ano: "))
			dados = self.m_rel_leituras.ConsolidadoPorAno(ano)
		else:
			dados = self.m_rel_leituras.ConsolidadoPorAno()

		tb = PrettyTable(['ANO', 'TOTAL'])

		for l in dados:
			tb.add_row([l['ano'],l['total']])

		print(tb.get_string())

	def LeiturasPorAno(self):
		ft_ano = str(input("Deseja filtrar por ano específico (s/n)?"))
		dados = ''
				
		if ft_ano == "s":
			ano = int(input("Digite o ano: "))
			dados = self.m_rel_leituras.LeiturasPorAno(ano)
		else:
			dados = self.m_rel_leituras.LeiturasPorAno()

		tb = PrettyTable(['TITULO', 'ANO'])

		for l in dados:
			tb.add_row([l['titulo'],l['ano']])

		print(tb.get_string())

	def ConsolidadoPorAutor(self):
		ft_autor = str(input("Deseja filtrar por autor específico (s/n)?"))
		dados = ''
				
		if ft_autor == "s":
			autores = self.m_autores.Listar()
			tb = PrettyTable(['ID', 'AUTOR'])

			for a in autores:
				pprint.pprint(a)
				tb.add_row([a[0], a[1]])

			print(tb.get_string())
			id_autor = int(input("Digite o id do autor: "))
			dados = self.m_rel_leituras.ConsolidadoPorAutor(id_autor = id_autor)
		else:
			dados = self.m_rel_leituras.ConsolidadoPorAutor()

		tb = PrettyTable(['AUTOR', 'TOTAL'])

		for l in dados:
			tb.add_row([l['autor'],l['total']])

		print(tb.get_string())

	def LeiturasPorAutor(self):
		ft_autor = str(input("Deseja filtrar por autor específico (s/n)?"))
		dados = ''
				
		if ft_autor == "s":
			autores = self.m_autores.Listar()
			tb = PrettyTable(['ID', 'AUTOR'])

			for a in autores:
				tb.add_row([a[0], a[1]])

			print(tb.get_string())
			id_autor = int(input("Digite o id do autor: "))
			dados = self.m_rel_leituras.LeiturasPorAutor(id_autor = id_autor)
		else:
			dados = self.m_rel_leituras.LeiturasPorAutor()

		tb = PrettyTable(['AUTOR', 'TITULO'])

		for l in dados:
			tb.add_row([l['autor'],l['titulo']])

		print(tb.get_string())

	def ConsolidadoPorEditora(self):
		ft_editora = str(input("Deseja filtrar por editora específica (s/n)?"))
		dados = ''
				
		if ft_editora == "s":
			editoras = self.m_editora.Listar()
			tb = PrettyTable(['ID', 'EDITORA'])

			for e in editoras:
				tb.add_row([e['id'], e['editora']])

			print(tb.get_string())
			id_editora = int(input("Digite o id da editora: "))
			dados = self.m_rel_leituras.ConsolidadoPorEditora(id_editora = id_editora)
		else:
			dados = self.m_rel_leituras.ConsolidadoPorEditora()

		tb = PrettyTable(['EDITORA', 'TOTAL'])

		for l in dados:
			tb.add_row([l['editora'],l['total']])

		print(tb.get_string())

	def LeiturasPorEditora(self):
		ft_editora = str(input("Deseja filtrar por editora específica (s/n)?"))
		dados = ''
				
		if ft_editora == "s":
			editoras = self.m_editora.Listar()
			tb = PrettyTable(['ID', 'EDITORA'])

			for e in editoras:
				tb.add_row([e['id'], e['editora']])

			print(tb.get_string())
			id_editora = int(input("Digite o id da editora: "))
			dados = self.m_rel_leituras.LeiturasPorEditora(id_editora = id_editora)
		else:
			dados = self.m_rel_leituras.LeiturasPorEditora()

		tb = PrettyTable(['EDITORA', 'TITULO'])

		for l in dados:
			tb.add_row([l['editora'],l['titulo']])

		print(tb.get_string())

	def ConsolidadoPorAssunto(self):
		ft_assunto = str(input("Deseja filtrar por assunto específico (s/n)?"))
		dados = ''
				
		if ft_assunto == "s":
			assuntos = self.m_assuntos.Listar()
			tb = PrettyTable(['ID', 'ASSUNTO'])

			for a in assuntos:
				tb.add_row([a[0], a[1]])

			print(tb.get_string())
			id_assunto = int(input("Digite o id do assunto: "))
			dados = self.m_rel_leituras.ConsolidadoPorAssunto(id_assunto = id_assunto)
		else:
			dados = self.m_rel_leituras.ConsolidadoPorAssunto()

		tb = PrettyTable(['ASSUNTO', 'TOTAL'])

		for l in dados:
			tb.add_row([l['assunto'],l['total']])

		print(tb.get_string())

	def LeiturasPorAssunto(self):
		ft_assunto = str(input("Deseja filtrar por assunto específico (s/n)?"))
		dados = ''
				
		if ft_assunto == "s":
			assuntos = self.m_assuntos.Listar()
			tb = PrettyTable(['ID', 'ASSUNTO'])

			for a in assuntos:
				tb.add_row([a[0], a[1]])

			print(tb.get_string())
			id_assunto = int(input("Digite o id do assunto: "))
			dados = self.m_rel_leituras.LeiturasPorAssunto(id_assunto = id_assunto)
		else:
			dados = self.m_rel_leituras.LeiturasPorAssunto()

		tb = PrettyTable(['ASSUNTO', 'TÍTULO'])

		for l in dados:
			tb.add_row([l['assunto'],l['titulo']])

		print(tb.get_string())

	def ConsolidadoPorFormato(self):
		dados = self.m_rel_leituras.ConsolidadoPorFormato()

		tb = PrettyTable(['FORMATO', 'TOTAL'])

		for f in dados:
			tb.add_row([f['formato'],f['total']])

		print(tb.get_string())

	def LeiturasPorFormato(self):
		ft_formato = str(input("Deseja filtrar por formato específico (s/n)?"))
		dados = ''
				
		if ft_formato == "s":
			formato = int(input("Digite o código do formato (1 - Digital, 0 - Fisíco): "))
			dados = self.m_rel_leituras.LeiturasPorFormato(formato = formato)
		else:
			dados = self.m_rel_leituras.LeiturasPorFormato()

		tb = PrettyTable(['TÍTULO', 'FORMATO'])

		for l in dados:
			tb.add_row([l['titulo'],l['formato']])

		print(tb.get_string())

	def ConsolidadoPorTipo(self):
		dados = self.m_rel_leituras.ConsolidadoPorTipo()

		tb = PrettyTable(['TIPO', 'TOTAL'])

		for t in dados:
			tb.add_row([t['tipo'],t['total']])

		print(tb.get_string())

	def LeiturasPorTipo(self):
		ft_tipo = str(input("Deseja filtrar por tipo específico (s/n)?"))
		dados = ''
				
		if ft_tipo == "s":
			tipo = int(input("Digite o código do tipo (1 - Digital, 2 - HQ, 3 - Mangá): "))
			dados = self.m_rel_leituras.LeiturasPorTipo(id_tipo = tipo)
		else:
			dados = self.m_rel_leituras.LeiturasPorTipo()

		tb = PrettyTable(['TÍTULO', 'TIPO'])

		for l in dados:
			tb.add_row([l['titulo'],l['tipo']])

		print(tb.get_string())

	def ListagemGeral(self):
		dados = self.m_rel_leituras.ListagemGeral()

		tb = PrettyTable(['TITULO', 'AUTOR', 'EDITORA', 'TIPO', 'DATA'])

		for i in dados:
			tb.add_row([i['titulo'], i['autor'], i['editora'], i['tipo'], i['dt']])

		print(tb.get_string())