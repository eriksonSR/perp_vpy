from controllers.controllers import *

msg = """\
Dite o código da entidade desejada:
1 - Leituras;
2 - Games;
3 - Autores;
4 - Editoras;
5 - Assuntos;
6 - Generos;
7 - Studios;
8 - Consoles;
9 - Relatórios Games;
10 - Relatórios Leituras;
0 - Sair
"""

op = ''
while op != 0:
	op = int(input(msg))
	if op == 1:
		c_leituras = LeiturasController()
	elif op == 2:
		c_games = GamesController()
	elif op == 3:
		c_autores = AutoresController()
	elif op == 4:
		c_editoras = EditorasController()
	elif op == 5:
		c_assuntos = AssuntosController()
	elif op == 6:
		c_generos = GenerosController()
	elif op == 7:
		c_studios = StudiosController()
	elif op == 8:
		c_consoles = ConsolesController()
	elif op == 9:
		c_rel_games = RelatoriosGamesController()
	elif op == 10:
		c_rel_leituras = RelatoriosLeiturasController()