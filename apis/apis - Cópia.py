import requests, json, xml.etree.ElementTree as ET
from pprint import pprint

class GamesDataBaseApi():
	def __init__(self, id_game):
		#Diretórios
		self.dir_xmls = './dados_apis/gamesdb/xml/'
		self.dir_xmls_infos = './dados_apis/gamesdb/xml/infos_games/'
		self.dir_xmls_imgs = './dados_apis/gamesdb/xml/infos_img_games/'
		#Urls
		self.url_base_imgs = 'http://legacy.thegamesdb.net/banners/'
		self.url_api_arts = 'http://thegamesdb.net/api/GetArt.php?id={}'.format(id_game)
		self.url_api_game = 'http://thegamesdb.net/api/GetGame.php?id={}'.format(id_game)
		#Dados game
		self.id_game = id_game
		self.infos_game = {'plataforma':'', 'lancamento':'', 'genero':[], 'descricao':'', 'publicadora':'', 'desenvolvedora':''}
		#Xmls
		self.arq_xml_info = '{}{}.xml'.format(self.dir_xmls_infos, self.id_game)
		self.arq_xml_art = '{}{}.xml'.format(self.dir_xmls_imgs, self.id_game)
	
	def GetXmlArts(self):
		#Conteúdo da url
		url_content = requests.get(self.url_api_arts).content
		#Converte conteúdo da url para xml
		root = ET.fromstring(url_content)
		arq_xml = r'{}/{}.xml'.format(self.dir_xmls_imgs, self.id_game)

		with open(arq_xml, "w") as f:
			f.write(url_content.decode("utf-8"))
	
	def GetXmlInfos(self):
		#Conteúdo da url
		url_content = requests.get(self.url_api_game).content
		#Converte conteúdo da url para xml
		root = ET.fromstring(url_content)
		arq_xml = r'{}/{}.xml'.format(self.dir_xmls_infos, self.id_game)

		with open(arq_xml, "w") as f:
			f.write(url_content.decode("utf-8"))

	def GetInfosGame(self):
		game_d = {'id_gdb':0, 'sinopse':'', 'lancamento':'', 'plataforma':'','titulo_original':'', 'publicadora':'','generos':[]}
		
		xml = ET.parse(self.arq_xml_info)
		root = xml.getroot()
		game = root.find('Game')
		
		if game.find('GameTitle') != None:
			game_d['titulo_original'] = game.find('GameTitle').text
		
		if game.find('Platform') != None:
			game_d['plataforma'] = game.find('Platform').text
		
		if game.find('ReleaseDate') != None:
			game_d['lancamento'] = game.find('ReleaseDate').text
		
		if game.find('Overview') != None:
			game_d['sinopse'] = game.find('Overview').text
		
		if game.find('Publisher') != None:
			game_d['publicadora'] = game.find('Publisher').text

		if game.find('Genres') != None:
			generos = game.find('Genres')
			for genero in generos.findall('genre'):
				game_d['generos'].append(genero.text)

		return game_d

	def GetBoxArtsImgs(self):
		dir_box = './imgs/games/boxarts/'
		dir_thumbs = './imgs/games/boxarts/thumbs/'

		xml = ET.parse(self.arq_xml_art)
		root = xml.getroot()

		imagens = root.find('Images')
		for boxart in imagens.findall('boxart'):
			#Dados da boxart
			img_data = requests.get('{}{}'.format(self.url_base_imgs, boxart.text)).content
			img_nome = boxart.text.split('/')
			img_nome = img_nome[-1].split('.')
			img_nome = '{}.{}'.format(img_nome[0], img_nome[1])
			caminho_boxart = '{}{}/{}'.format(dir_box, boxart.get('side'), img_nome)

			#Salva a boxart
			with open(caminho_boxart, 'wb') as handler:
				handler.write(img_data)

			#Dados da thumb
			img_thumb_data = requests.get('{}{}'.format(self.url_base_imgs, boxart.get('thumb'))).content
			img_thumb_nome = boxart.get('thumb').split('/')
			img_thumb_nome = img_thumb_nome[-1].split('.')
			img_thumb_nome = '{}.{}'.format(img_thumb_nome[0], img_thumb_nome[1])
			caminho_thumb = '{}{}/{}'.format(dir_thumbs, boxart.get('side'), img_thumb_nome)

			#Salva a thumb
			with open(caminho_thumb, 'wb') as handler:
				handler.write(img_thumb_data)

	def GetClearlogos(self):
		dir_clearlogos = './imgs/games/clearlogos/'
		
		xml = ET.parse(self.arq_xml_art)
		root = xml.getroot()

		imagens = root.find('Images')
		for clearlogo in imagens.findall('clearlogo'):
			#Dados do logo
			img_data = requests.get('{}{}'.format(self.url_base_imgs, clearlogo.text)).content
			img_nome = clearlogo.text.split('/')
			img_nome = img_nome[-1].split('.')
			img_nome = '{}.{}'.format(img_nome[0], img_nome[1])
			caminho_boxart = '{}{}'.format(dir_clearlogos, img_nome)

			#Salva o logo
			with open(caminho_boxart, 'wb') as handler:
				handler.write(img_data)

	def GetFanarts(self):
		dir_originais = './imgs/games/fanarts/originais/'
		dir_thumbs = './imgs/games/fanarts/thumbs/'

		xml = ET.parse(self.arq_xml_art)
		root = xml.getroot()

		imagens = root.find('Images')
		for fanart in imagens.findall('fanart'):
			#Dados da fanart
			original = fanart.find('original')
			img_data = requests.get('{}{}'.format(self.url_base_imgs, original.text)).content
			img_nome = original.text.split('/')
			img_nome = img_nome[-1].split('.')
			img_nome = '{}.{}'.format(img_nome[0], img_nome[1])
			caminho_original = '{}{}'.format(dir_originais, img_nome)

			#Salva a fanart
			with open(caminho_original, 'wb') as handler:
				handler.write(img_data)

			#Dados da thumb
			thumb = fanart.find('thumb')
			img_thumb_data = requests.get('{}{}'.format(self.url_base_imgs, thumb.text)).content
			img_thumb_nome = thumb.text.split('/')
			img_thumb_nome = img_thumb_nome[-1].split('.')
			img_thumb_nome = '{}.{}'.format(img_thumb_nome[0], img_thumb_nome[1])
			caminho_thumb = '{}{}'.format(dir_thumbs, img_thumb_nome)

			#Salva a thumb
			with open(caminho_thumb, 'wb') as handler:
				handler.write(img_thumb_data)

	def GetScreenshots(self):
		dir_originais = './imgs/games/screenshots/originais/'
		dir_thumbs = './imgs/games/screenshots/thumbs/'

		xml = ET.parse(self.arq_xml_art)
		root = xml.getroot()

		imagens = root.find('Images')
		for fanart in imagens.findall('screenshot'):
			#Dados da fanart
			original = fanart.find('original')
			img_data = requests.get('{}{}'.format(self.url_base_imgs, original.text)).content
			img_nome = original.text.split('/')
			img_nome = img_nome[-1].split('.')
			img_nome = '{}.{}'.format(img_nome[0], img_nome[1])
			caminho_original = '{}{}'.format(dir_originais, img_nome)

			#Salva a fanart
			with open(caminho_original, 'wb') as handler:
				handler.write(img_data)

			#Dados da thumb
			thumb = fanart.find('thumb')
			img_thumb_data = requests.get('{}{}'.format(self.url_base_imgs, thumb.text)).content
			img_thumb_nome = thumb.text.split('/')
			img_thumb_nome = img_thumb_nome[-1].split('.')
			img_thumb_nome = '{}.{}'.format(img_thumb_nome[0], img_thumb_nome[1])
			caminho_thumb = '{}{}'.format(dir_thumbs, img_thumb_nome)

			#Salva a thumb
			with open(caminho_thumb, 'wb') as handler:
				handler.write(img_thumb_data)

class SkoobApi():
	def __init__(self, id_livro):
		self.id_user = 1026257
		self.id_livro = id_livro
		self.url_api = 'https://www.skoob.com.br/v1/book/{}/user_id:{}/'.format(id_livro, self.id_user)
		self.dir_json = './dados_apis/skoob/json/'

	def GetJson(self):
		sk_json = requests.get(self.url_api).json()
		arq = r'{}{}.json'.format(self.dir_json, self.id_livro)
		
		with open(arq, 'w') as arq_j:
			json.dump(sk_json, arq_j)

class IgdbApi():
	def __init__(self, id_game):
		self.id_game = id_game
		self.dir_json = './dados_apis/igdb/json/'
		self.arq_json = '{}{}.json'.format(self.dir_json, self.id_game)
		self.api_key = '07c45d0dc693222ccb9e837f4e8d400d'
		self.api_headers = {'user-key':self.api_key, 'Accept':'application/json'}
		self.url_api_game = 'https://api-endpoint.igdb.com/games/'
		self.url_img_original_game = 'https://images.igdb.com/igdb/image/upload/t_1080p/'
		self.url_img_thumb_game = 'https://images.igdb.com/igdb/image/upload/t_thumb/'

	def GetJson(self):
		fields = '?fields=id,name,url,summary,developers,publishers,dlcs,release_dates,websites,alternative_names,screenshots,artworks,videos,cover,websites,genres.name,game.name,developers.name,publishers.name&expand=game,genres,developers,publishers'
		j = requests.get('{}{}/{}'.format(self.url_api_game,self.id_game,fields), headers = self.api_headers).json()

		arq = r'./dados_apis/igdb/json/{}.json'.format(self.id_game)
		with open(arq, 'w') as arq_j:
			json.dump(j, arq_j)

	def GetBoxArtsUrls(self):
		urls_boxes = {'originais':[],'thumbs':[]}
		arq = r'./dados_apis/igdb/json/{}.json'.format(self.id_game)

		with open(arq, 'r') as arq_j:
			j = json.load(arq_j)
			id_img = j[0]['cover']['cloudinary_id']
			
			urls_boxes['originais'].append('{}{}.jpg'.format(self.url_img_original_game, id_img))
			urls_boxes['thumbs'].append('{}{}.jpg'.format(self.url_img_thumb_game, id_img))

		return urls_boxes

	def GetScreenshotsUrls(self):
		urls_screenshots = {'originais':[],'thumbs':[]}
		arq = r'./dados_apis/igdb/json/{}.json'.format(self.id_game)

		with open(arq, 'r') as arq_j:
			j = json.load(arq_j)
			for scr in j[0]['screenshots']:
				id_img = scr['cloudinary_id']
				urls_screenshots['originais'].append('{}{}.jpg'.format(self.url_img_original_game, id_img))
				urls_screenshots['thumbs'].append('{}{}.jpg'.format(self.url_img_thumb_game, id_img))

		return urls_screenshots

	def GetArtWorksUrls(self):
		urls_artworks = {'originais':[],'thumbs':[]}
		arq = r'./dados_apis/igdb/json/{}.json'.format(self.id_game)

		with open(arq, 'r') as arq_j:
			j = json.load(arq_j)

			if 'artworks' in j[0].keys():
				for art in j[0]['artworks']:
					id_img = art['cloudinary_id']
					urls_artworks['originais'].append('{}{}.jpg'.format(self.url_img_original_game, id_img))
					urls_artworks['thumbs'].append('{}{}.jpg'.format(self.url_img_thumb_game, id_img))

		return urls_artworks